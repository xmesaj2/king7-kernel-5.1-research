struct LCM_setting_table{
    unsigned int cmd;
    unsigned char count;
    unsigned char para_list[];
};

struct LCM_DSI_PARAMS
{
    LCM_DSI_MODE_CON 	mode;
	LCM_DSI_MODE_CON 	switch_mode;
    unsigned int		DSI_WMEM_CONTI;
    unsigned int		DSI_RMEM_CONTI;	
    unsigned int		VC_NUM;
    
    LCM_LANE_NUM		LANE_NUM;
    LCM_DSI_DATA_FORMAT	data_format;

    /* intermediate buffers parameters */
    unsigned int 		intermediat_buffer_num; // 2..3
    
    LCM_PS_TYPE			PS;	
    unsigned int		word_count;
    
    unsigned int		packet_size;
    unsigned int        packet_size_mult;
    unsigned int		vertical_sync_active;
    unsigned int		vertical_backporch;
    unsigned int		vertical_frontporch;
	unsigned int		vertical_frontporch_for_low_power;
    unsigned int		vertical_active_line;
    
    unsigned int		horizontal_sync_active;
    unsigned int		horizontal_backporch;
    unsigned int		horizontal_frontporch;
    unsigned int		horizontal_blanking_pixel;
    unsigned int		horizontal_active_pixel;
    unsigned int		horizontal_bllp;
    
    unsigned int		line_byte;
    unsigned int		horizontal_sync_active_byte;
    unsigned int		horizontal_backporch_byte;
    unsigned int		horizontal_frontporch_byte; 
    unsigned int		rgb_byte; 
    
    unsigned int		horizontal_sync_active_word_count;	
    unsigned int		horizontal_backporch_word_count;
    unsigned int		horizontal_frontporch_word_count;
    
    unsigned char		HS_TRAIL;
    unsigned char		HS_ZERO;
    unsigned char		HS_PRPR;
    unsigned char		LPX;
    
    unsigned char		TA_SACK;
    unsigned char		TA_GET;
    unsigned char		TA_SURE;	
    unsigned char		TA_GO;
    
    unsigned char		CLK_TRAIL;
    unsigned char		CLK_ZERO;	
    unsigned char		LPX_WAIT;
    unsigned char		CONT_DET;
    
    unsigned char		CLK_HS_PRPR;
    unsigned char		CLK_HS_POST;
    unsigned char       DA_HS_EXIT;
    unsigned char       CLK_HS_EXIT;

    unsigned int		pll_select;
    unsigned int		pll_div1;
    unsigned int		pll_div2;	
    unsigned int        fbk_div;
    unsigned int		fbk_sel;
    unsigned int		rg_bir;
    unsigned int		rg_bic;
    unsigned int		rg_bp;
    unsigned int		PLL_CLOCK;
    unsigned int		dsi_clock;
    unsigned int		ssc_disable;
    unsigned int		ssc_range;
    unsigned int		compatibility_for_nvk;
    unsigned int		cont_clock;
    unsigned int		ufoe_enable;
    LCM_UFOE_CONFIG_PARAMS ufoe_params;
    unsigned int		edp_panel;
    unsigned int                customization_esd_check_enable;
    unsigned int                esd_check_enable;
    unsigned int		lcm_int_te_monitor;
    unsigned int		lcm_int_te_period;
    
    unsigned int		lcm_ext_te_monitor;
    unsigned int		lcm_ext_te_enable;
    
    unsigned int		noncont_clock;
    unsigned int		noncont_clock_period;
    unsigned int		clk_lp_per_line_enable;
    LCM_esd_check_item          lcm_esd_check_table[3];
    unsigned int switch_mode_enable;
	DUAL_DSI_TYPE       dual_dsi_type;    
	unsigned int			lane_swap_en;
	MIPITX_PHY_LANE_SWAP lane_swap[MIPITX_PHY_PORT_NUM][MIPITX_PHY_LANE_NUM];

    unsigned int        vertical_vfp_lp;
    
#ifdef CONFIG_MIXMODE_FOR_INCELL
    unsigned int		mixmode_enable;
    unsigned int		mixmode_mipi_clock; 
    unsigned int		pwm_fps;
#endif
	unsigned int        lfr_enable;
	unsigned int        lfr_mode;
	unsigned int        lfr_type;
	unsigned int        lfr_skip_num;

    unsigned int ext_te_edge;

};

struct LCM_PARAMS
{
  LCM_TYPE type;
  LCM_CTRL ctrl;
  LCM_INTERFACE_ID lcm_if;
  LCM_INTERFACE_ID lcm_cmd_if;
  unsigned int width;
  unsigned int height;
  unsigned int io_select_mode;
  LCM_DBI_PARAMS dbi;
  LCM_DPI_PARAMS dpi;
  LCM_DSI_PARAMS dsi;
  unsigned int physical_width;
  unsigned int physical_height;
  unsigned int od_table_size;
  void *od_table;
};
