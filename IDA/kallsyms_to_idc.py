import re
import os
import sys


def usage():
    print("%s <path to kallsyms>" % sys.argv[0] )
    print("%s /proc/kallsyms" % sys.argv[0] )


def main():
    prefix = """
#define UNLOADED_FILE   1
#include <idc.idc>

static main(void)
{
    """

    suffix = '}'
    frmat = '\tMakeName(0x%s, "RE_%s__%s");'

    regex = re.compile( r'([a-fA-Z0-9]+) \w (\w+)' )


    #if( len(sys.argv) != 2 ):
    #    return usage()

    kallsymsPath = "kallsyms.txt"

    fk = open( kallsymsPath, "r" )
    if None == fk:
        print("Unable to open %s.\n", kallsymsPath )
        return -3


    print( "// Enabling addresses...")
    #cmd = 'echo 1 > /proc/sys/kernel/kptr_restrict'
    #os.system( cmd )
    
    print(prefix)
    lines = fk.readlines()
    for line in lines:
        match = regex.match(line)
        if match != None:
            str1 = match.group(1)
            str2 = match.group(2)
            print(frmat % (str1, str2, str1))

    fk.close()

    print(suffix)
    return 0
  
if __name__ == '__main__':
    main()
 