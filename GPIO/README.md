Notes

| device | driver stock | i2c bus stock | driver port | i2c bus port | driver l861 | i2c bus l861 |
|--- |--- |--- |--- |--- |--- |--- |
| touchpanel | g9xx | 2-005D | g1x | 2-005D | - | -|
| touchscreen | FT | 2-0038 | - | - | gt9xx | 2-005D |
| accelerometer | lsm6ds0_acc | 3-006B | ACCELEROMETER | - | KXTIK1013| 3-000E |
| als/ps | stk3x1x | 3-0048 | LIGHT | - | stk3x1x | 3-0048 |
| barometer | PRESSURE | - | PRESSURE | - | PRESSURE | - |
| magnetometer | mmc3524x | 3-0030 | MAGNETOMETER | - | akm09911 |3-000C |
| gyroscope | lsm6ds0 | 3-006A | GYROSCOPE | - | MPU3000 | 3-0068 |
| camera | ov8858_mipi_raw ov8858sy_mipi_raw s5k3m2_ofg_mipi_raw s5k3m2_sy_mipi_raw | - | ov8858_mipi_raw ov8858sy_mipi_raw s5k3m2_ofg_mipi_raw s5k3m2_sy_mipi_raw | - | imx230_mipi_raw ov8858_mipi_raw | - |
| lens af | DW9800AF BU6429AF | 0-0018 0-0019 | DW9714AF LC898122AF LC898212AF AD5820AF | 0-0018 0-0048 0-0022 0-006C | DW9714AF | 0-0018 |
| charger | USE PMIC | - | USE PMIC | - | USE PMIC | - |
| PMIC | da9210 tps6128x tps65132 | 1-0068 1-0075 2-003E | da9210 tps6128x tps65132 | 1-0068 1-0075 0-003E | da9210 tps6128x | 1-0068 1-0075 |
| audio | TFA98XX es9018-codec | 2-0034 2-0048 | - | - | mtsndcard | - |
| other | CAM_CAL_DRV remote_ctrl kd_camera_hw kd_camera_kw_bus2 | 0-0050 3-0050 0-007F 3-007F | CAM_CAL_DRV kd_camera_hw lp3101 kd_camera_hw_bus2 | 0-0050 0-007F 2-003E 2-007F | - | - |
| nfc | - | - | - | - | mt6605 | 3-0028 |