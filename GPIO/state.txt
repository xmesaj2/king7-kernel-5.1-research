

Changes for 176 (Audio):
             FROM    TO
MODE         1       1
PULLSEL      0       0
DIN          0       1
DOUT         0       0
PULLEN       0       0
DIR          0       0
IES          1       1


Changes for 106 (Screen & Touchpanel):
             FROM    TO
MODE         0       0
PULLSEL      0       0
DIN          0       1
DOUT         0       1
PULLEN       0       0
DIR          1       1
IES          1       1


Changes for 130 (Screen & Touchpanel):
             FROM    TO
MODE         0       0
PULLSEL      0       0
DIN          0       1
DOUT         0       1
PULLEN       1       1
DIR          1       1
IES          1       1


Changes for 131 (Screen & Touchpanel):
             FROM    TO
MODE         0       0
PULLSEL      0       0
DIN          0       1
DOUT         0       1
PULLEN       0       0
DIR          1       1
IES          1       1


Changes for 185 (Screen & Touchpanel):
             FROM    TO
MODE         1       1
PULLSEL      0       0
DIN          1       0
DOUT         0       0
PULLEN       1       1
DIR          0       0
IES          1       1


Changes for 186 (Screen & Touchpanel):
             FROM    TO
MODE         1       1
PULLSEL      0       0
DIN          0       1
DOUT         1       1
PULLEN       0       0
DIR          1       1
IES          1       1


Changes for 177 (RIL (Will be broken after this, restart phone)):
             FROM    TO
MODE         1       1
PULLSEL      0       0
DIN          0       1
DOUT         0       0
PULLEN       0       0
DIR          0       0
IES          1       1


Changes for 178 (RIL (Will be broken after this, restart phone)):
             FROM    TO
MODE         1       1
PULLSEL      0       0
DIN          0       1
DOUT         0       0
PULLEN       0       0
DIR          0       0
IES          1       1
