# PPTV King7 Kernel research and tools

`boot` - unpacked Image to load with IDA

In IDA64 select `ARM Little-endian` then

in Create ROM Section

Set address to `0xffffffc000080000`

end address default

below

start address `0xffffffc000080000`

leave rest as defaults

after file loads run script `makecode.idc` then when finished `kallsyms.idc`

Symbols will load for easier file reading

[MEGA download link database file for IDA](https://mega.nz/#F!ecVhgaRK!Q50A5J_30YO7Lnzboh1kIw) I will upload and update my database here.

`/IDA`

scripts to generate `makecode.idc` and `kallsyms.idc` from `IDA/kallsyms.txt`

`/GPIO`

Stock pinout and driver information from MTK-Pin-Finder

`ProjectConfig.mk` - extracted from device stock software

`/carliv_image_kitchen`

tool to unpack/repack boot image after build (requires cpio installed in system, see carliv README for more information and required libs)

Unpack `boot-stock.img` with carliv
replace `_stock-boot.extraced/boot-stock.img-kernel` with build `zImage-dtb` (rename, must be `boot-stock.img-kernel`)
repack to obtain flashable `output/boot_YYYYMMDD_HHmm.img` for TWRP (Flash Image, not ZIP)

|KEY|VALUE|STATUS|
|--- |--- |--- |
|MANUFACTURER|PPTV|--- |
|BRAND|PPTV|--- |
|MODEL|KING 7|--- |
|PLATFORM|mt6795|--- |
|SOC|Helio X10 series|--- |
|RESOLUTION|2560x1440|--- |
|LCM|r63419_wqhd_truly_auo_vdo|partially works |
|TOUCHSCREEN|FT|gt1x driver found in stock, doesn't work |
|CAMERA|ov8858_mipi_raw ov8858sy_mipi_raw s5k3m2_ofg_mipi_raw s5k3m2_sy_mipi_raw|--- |
|LENS|DW9800AF BU6429AF|--- |
|SOUND|amp_6323pmic_spk|doesn't work |
|ACCELEROMETER|lsm6ds0_acc|--- |
|ALSPS|stk3x1x|--- |
|MAGNETOMETER|mmc3524x|--- |
|GYROSCOPE|lsm6ds0|--- |
|BAROMETER|PRESSURE|--- |
|CHARGER|USE PMIC|works |
|PMIC|da9210 tps6128x tps65132|--- |
|WIFI|MT6630|works |
|MODEM|dwav6795_lwt_l1_lttg dwav6795_lwt_l1_lwg|works |
|OTHER|CAM_CAL_DRV remote_ctrl kd_camera_hw kd_camera_hw_bus2|--- |
|CPU|MT6795|--- |
|CORES|8|--- |
|FAMILY|Cortex-A53|--- |
|CLOCK_SPEED|403 - 1950 MHz|--- |
|GPU_VENDOR|Imagination Technologies|--- |
|GPU_MODEL|PowerVR Rogue G6200|--- |
|GPU_CLOCK|253 - 676 MHz|--- |
|RAM|3 GB LPDDR3|--- |
|FLASH|BGND3R|--- |
|AUDIO|TFA98XX es9018-codec|doesn't work |
